function createCard(title, description, pictureUrl, location, starts, ends) {
    return `
        <div class="col">
            <div class="card mb-3 shadow">
                <img src="${pictureUrl}" class="card-img-top">
                <div class="card-body">
                    <h5 class="card-title">${title}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                    <p class="card-text">${description}</p>
                </div>
                <div class="card-footer">
                    ${new Date(starts).toLocaleDateString()} - 
                    ${new Date(ends).toLocaleDateString()}
                </div>
            </div>
        </div>
    `;
}


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
  
    try {
      const response = await fetch(url);
  
      if (!response.ok) {
        alert("Unsuccessful response, check URL");
      } else {
            const data = await response.json();
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const location = details.conference.location.name
                    const starts = details.conference.starts;
                    const ends = details.conference.ends;
                    const html = createCard(title, description, pictureUrl, location, starts, ends);
                    const row = document.querySelector('.row');
                    row.innerHTML += html;
                }
            }
        }
    } catch (e) {
      console.log("Error 404: Data not found, check code", e)
    }
  
});
